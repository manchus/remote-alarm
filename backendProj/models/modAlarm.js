const mongoose = require('mongoose');

const alarmSchema = new mongoose.Schema({
    idEvent: String,
    time: String,
    event: String
});

module.exports = mongoose.model('alarm', alarmSchema);
