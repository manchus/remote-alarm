const mongoose = require('mongoose');

const contSchema = new mongoose.Schema({
    _id: String,
    sequence_value: Number,
});

module.exports = mongoose.model('Counters', contSchema);

