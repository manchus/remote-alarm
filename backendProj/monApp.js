const express = require('express') 
// express app
const app = express() 
const mongoose = require('mongoose');
const connection = mongoose.connection;
//mongo DB
const  bodyParser = require('body-parser');
//bodyParser nous permet de lire les objects JSON
const Alarm = require('./models/modAlarm');
const Counters = require('./models/cont');
//Nous importons le modèle de données 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
//Permet de connecter Node et Angular 
const cors = require('cors');

app.use(cors());

//Connection to DataBase
mongoose.connect('mongodb://localhost:27017/bdAlarm',(
  {
    useUnifiedTopology: true, 
    useNewUrlParser: true,
    useFindAndModify: false 
  }));
connection.once('open',()=>{
  console.log('Connected to MongoDB');
});

//GET - SELECT ALL
app.get('/events', (req,res) =>{
  Alarm.find().exec()  //se mettre en attente de la réponse de la BD.
   .then(alarm => res.status(200).json(alarm));
});


  app.post('/addEvt', (req,res) =>{
   var aa;
   Counters.findOneAndUpdate({"_id": "productid"}, {$inc:{sequence_value: 1 }}).exec()  //se mettre en attente de la réponse de la BD.
   .then( counters => {
      const alarm = new Alarm({
      idEvent : counters.sequence_value,
      time: Date.now(),   //req.body.time,
      event: req.body.event});
      alarm.save((err, alarm)=>{
      //alarm.insertOne((err, alarm)=>{
        if(err){
          return res.status(500).json(err);
        }
        res.status(201).json(alarm);
      })
   })   
  });

  //app.post('/getEvt', (req,res) =>{
  app.post('/getEvt', (req,res) =>{
    Alarm.findOne().sort({_id:-1}).limit(1).exec()  //se mettre en attente de la réponse de la BD.
    .then( alarm => {
      console.log(alarm.event);
      res.status(200).json(alarm.event);
    } )   
   });
 

  

  app.listen(3000, ()=> { 
    console.log("j'écoute le port 3000!");
  });
  