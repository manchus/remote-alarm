from tkinter import *
from tkinter import ttk
import threading
from led2_Obj import circuit
from connect import requete
from time import sleep


class Fenetre:
    activated = False
    root = Tk()
    content = ttk.Frame(root)
    frame = ttk.Frame(content, borderwidth=5, relief="ridge", width=400, height=300)
    lblAlarm = ttk.Label(content, text="ALARMS")
    lblStatus = ttk.Label(content, text="STATUS")
    btnZ1 = Button(content, text="Z1", bg='grey',  state='disabled')
    btnZ2 = Button(content, text="Z2", bg='grey',  state='disabled')
    btnZ3 = Button(content, text="Z3", bg='grey',  state='disabled')
    btnZ4 = Button(content, text="Z4", bg='grey',  state='disabled')
    btnStatus = Button(content, text="ON", bg='red')
    btnActivate = ttk.Button(content, text="  ACTIVATE  ", state='disabled')
    btnDeactivate = ttk.Button(content, text="DEACTIVATE", state='disabled')
    btnReset = ttk.Button(content, text="RESET ALARM", state='disabled')   
    mycircuit = circuit()
    
    def windowbuild(self):
        self.content.grid(column=0, row=0)
        self.lblAlarm.grid(column=0, row=0, columnspan=2)
        self.btnActivate.grid(column=3, row=1, columnspan=2, ipadx=50, ipady=20)
        self.btnZ1.grid(column=0, row=1,rowspan=2, ipadx=40, ipady=40)
        self.btnZ2.grid(column=1, row=1,rowspan=2, ipadx=40, ipady=40)
        self.btnZ3.grid(column=0, row=3,rowspan=2, ipadx=40, ipady=40)
        self.btnZ4.grid(column=1, row=3,rowspan=2, ipadx=40, ipady=40)
        self.btnDeactivate.grid(column=3, row=3, columnspan=2, ipadx=50, ipady=20)
        self.lblStatus.grid(column=0, row=3, columnspan=2)
        self.btnStatus.grid(column=0, row=5, columnspan=2, ipadx=40, ipady=15)
        self.btnReset.grid(column=3, row=5, columnspan=2, ipadx=50, ipady=20)
        self.root.mainloop()

    @classmethod
    def zone1(self):
        if self.activated == False:
            self.mycircuit.actived(1)
            print("echo Zone 1 Actived")
            self.btnStatus.configure(text='ON',bg='red')
            self.btnZ1.configure(state='normal', bg='red')
            self.btnZ2.configure(state='disabled', bg='grey')
            self.btnZ3.configure(state='disabled', bg='grey')
            self.btnZ4.configure(state='disabled', bg='grey')
            self.activated = True
        else:
            print("echo Zone 1 Unactived")
            self.deactivate_alarm(self)

    def zone2(self):
        if self.activated == False:
            self.mycircuit.actived(2)
            print("echo Zone 2 Actived")
            self.btnStatus.configure(text='ON',bg='red')
            self.btnZ1.configure(state='disabled', bg='grey')
            self.btnZ2.configure(state='normal', bg='red')
            self.btnZ3.configure(state='disabled', bg='grey')
            self.btnZ4.configure(state='disabled', bg='grey')
            self.activated = True
        else:
            print("echo Zone 1 Unactived")
            self.deactivate_alarm()

    def zone3(self):
        if self.activated == False:
            self.mycircuit.actived(3)
            print("echo Zone 1 Actived")
            self.btnStatus.configure(text='ON',bg='red')
            self.btnZ1.configure(state='disabled', bg='grey')
            self.btnZ2.configure(state='disabled', bg='grey')
            self.btnZ3.configure(state='normal', bg='red')
            self.btnZ4.configure(state='disabled', bg='grey')
            self.activated = True
        else:
            print("echo Zone 3 Unactived")
            self.deactivate_alarm()
        
    def zone4(self):
        if self.activated == False:
            self.mycircuit.actived(4)
            print("echo Zone 1 Actived")
            self.btnStatus.configure(text='ON',bg='red')
            self.btnZ1.configure(state='disabled', bg='grey')
            self.btnZ2.configure(state='disabled', bg='grey')
            self.btnZ3.configure(state='disabled', bg='grey')
            self.btnZ4.configure(state='normal', bg='red')
            self.activated = True
        else:
            print("echo Zone 4 Unactived")
            self.deactivate_alarm()
        
    def status(self):
        if self.btnStatus['text'] == 'ON':
            self.btnStatus.configure(text='OFF', bg='green')
            self.btnZ1.configure( bg='green')
            self.btnZ2.configure( bg='green')
            self.btnZ3.configure( bg='green')
            self.btnZ4.configure( bg='green')
            self.btnActivate.configure(state='normal')
            self.btnReset.configure(state='normal')
        else:
            self.btnStatus.configure(text='ON',bg='red')
            self.btnZ1.configure(state='disabled', bg='grey')
            self.btnZ2.configure(state='disabled', bg='grey')
            self.btnZ3.configure(state='disabled', bg='grey')
            self.btnZ4.configure(state='disabled', bg='grey')
            self.btnDeactivate.configure(state='disabled')
            self.btnActivate.configure(state='disabled')
            self.btnReset.configure(state='disabled')

    def activate_alarm(self):
        self.mycircuit.arm()
        self.btnActivate.configure(state='disabled')
        self.btnDeactivate.configure(state='normal')
        self.btnZ1.configure(state='normal', bg='green')
        self.btnZ2.configure(state='normal', bg='green')
        self.btnZ3.configure(state='normal', bg='green')
        self.btnZ4.configure(state='normal', bg='green')

    def deactivate_alarm(self):
        self.activated = False
        self.mycircuit.arm()
        self.btnActivate.configure(state='normal')
        self.btnDeactivate.configure(state='disabled')
        self.btnZ1.configure(state='disabled', bg='green')
        self.btnZ2.configure(state='disabled', bg='green')
        self.btnZ3.configure(state='disabled', bg='green')
        self.btnZ4.configure(state='disabled', bg='green')


    def reset(self):
        self.btnStatus.configure(text='OFF')
        self.btnZ1.configure(bg='green')
        self.btnZ2.configure(bg='green')
        self.btnZ3.configure(bg='green')
        self.btnZ4.configure(bg='green')
        self.btnActivate.configure(state='normal')
        self.btnDeactivate.configure(state='disabled')

    def __init__(self):
        self.btnZ1.configure(command=self.zone1)
        self.btnZ2.configure(command=self.zone2)
        self.btnZ3.configure(command=self.zone3)
        self.btnZ4.configure(command=self.zone4)
        self.btnStatus.configure(command=self.status)
        self.btnActivate.configure(command=self.activate_alarm)
        self.btnDeactivate.configure(command=self.deactivate_alarm)
        self.btnReset.configure(command=self.reset)
        self.windowbuild()
        
        
if __name__=="__main__":
    aa = Fenetre()
